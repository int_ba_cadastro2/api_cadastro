from core.configs import settings

from sqlalchemy import Column, Integer, String

#tabela e modelo do banco 
class CadastroModel(settings.DBBaseModel):
    __tablename__ = 'cadastro'

    id: int = Column(Integer, primary_key=True, autoincrement=True)
    nome: str = Column(String(100))
    email: str = Column(String(100))
    endereco: str = Column(String(150))
  
